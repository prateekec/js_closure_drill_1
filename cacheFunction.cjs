function cacheFunction(cb) {
    if(cb ==undefined){
        throw new Error("Data is not provided");
    }

    let cache={};
    let ans=function(){
        let args=Object.values(arguments);
        console.log(typeof(args));
        console.log(args);
        // console.log(args);


        let key="";
        for(let idx in args){
            key+=args[idx];
        }
        // console.log("KEY", key);


        if(cache[key]!=undefined){
            console.log("This is from cache");
            return cache[key];
        }
        else{
            cache[key]=cb(...args);
            console.log("This is from cb function");
            return cache[key];
        }
    }
    return ans;
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
}
module.exports=cacheFunction;