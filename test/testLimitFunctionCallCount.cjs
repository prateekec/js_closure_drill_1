const limitFunctionCallCount=require('../limitFunctionCallCount.cjs');

function cb(argu,argy){
    console.log(argu,argy);
}

let fun1=limitFunctionCallCount(cb,5);
fun1(1,6);
fun1(2,5);
fun1(3,4);
fun1(4,3);
fun1(5,2);

fun1();