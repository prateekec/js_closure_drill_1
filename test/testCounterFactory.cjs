const counterFactory=require('../counterFactory.cjs');


let counterA=counterFactory();
let counterB=counterFactory();

console.log(counterA.decrement());
console.log(counterA.decrement());
console.log(counterA.decrement());
console.log(counterA.decrement());
console.log(counterA.decrement());
console.log(counterA.decrement());
console.log(counterA.decrement());

console.log(counterB.increment());
console.log(counterB.increment());
console.log(counterB.increment());
console.log(counterB.increment());
console.log(counterB.increment());
console.log(counterB.increment());
console.log(counterB.increment());
