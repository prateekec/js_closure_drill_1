const cacheFunction=require('../cacheFunction.cjs');

function cb(a,b,c,d){
    console.log(arguments[0]);
    return a+b+c+d;
}

let fun1=cacheFunction(cb);
let fun2=cacheFunction(cb);
let fun3=cacheFunction(cb);

console.log(fun1());
// console.log(fun2(2));
// console.log(fun3(3));


// console.log(fun1(1));
// console.log(fun2(2));
// console.log(fun3(3));