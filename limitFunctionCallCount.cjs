function limitFunctionCallCount(cb, n) {
    if(cb==undefined || n==undefined){
        throw new Error("Data is not provided");
    }

    let func= function (){
        if(n==0){
            // console.log("No more calls");
            return null;
        }
        else{
            n-=1;
            return cb.apply(null,Array.prototype.slice.call(arguments));
        }
    }
    return func;
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
}
module.exports=limitFunctionCallCount;